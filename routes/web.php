<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/', 'PublicController@index');
Route::get('/welcome', 'PublicController@index');


//functions

Route::get('/search','PublicController@search');
Route::get('/delete-order/{oid}','PublicController@deleteOrder');
Route::get('logout','PublicController@logout');
Route::post('confirmation-email','PublicController@confirmationMail');



//Main

Route::get('/order','PublicController@order');
Route::get('/track','PublicController@track');
Route::get('/update','PublicController@update');



//Upload pages

Route::post('upload-order', 'PublicController@postUploadOrder');

Route::post('/update-order', 'PublicController@updateOrder');


//Admin side (modified by toby)

Route::get('admin-dashboard','PublicController@admin');
Route::get('update-order/{oid}','PublicController@updateOrder');

Route::post('update-order','PublicController@postUpdateOrder');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
