<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="author" content="Sebastian Alsina" />

    <title>Uber Referrals &bull; KissMyAxe</title>

    <!-- CSS -->

    <link type="text/css" rel="stylesheet" href="css/pro.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



</head>

<body style="background-image: url('img/bg.jpg');">


<nav class="navbar transparent">
    <form class="form-inline">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth

                    <button class="btn btn-outline-success"  href="{{ url('/welcome') }}">Home</button>



                @else

                        <button class="btn btn-outline-success"  href="{{ url('login') }}">Home</button>

                    @endauth
            </div>
        @endif
    </form>
</nav>
<section class="team-section py-5">
    <div class="container">
        <div class="row justify-content-center">


            <div class="col-md-4">
                <div class="card card-dark" style="width:300px; height:400px;">
                    <!--Card image-->
                    <div class="view overlay">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2821%29.jpg" class="img-fluid" alt="work desk">
                        <a>
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-body elegant-color white-text">
                        <!--Social shares button-->
                        <a class="activator p-3 mr-2"><i class="fa fa-share-alt white-text"></i></a>
                        <!--Title-->
                        <h4 class="card-title">Order</h4>
                        <hr class="hr-light">
                        <!--Text-->
                        <p class="font-small mb-4">
                            Haven't ordered uber rides yet? Do so here! If unsure, you'll have the option to get in touch with me via skype.
                        </p>
                       
                            <a type="submit" href="{{url('/order')}}" class="btn blue-gradient btn-rounded">Order<span><i class="fa fa-chevron-right pl-2"></i></span></a>

                    

                    </div>
                    <!--/.Card content-->
                </div>

            </div>

            <div class="col-md-4">
                <div class="card card-indigo" style="width:300px; height:400px;">
                    <!--Card image-->
                    <div class="view overlay">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2821%29.jpg" class="img-fluid" alt="work desk">
                        <a>
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-body elegant-color white-text">
                        <!--Social shares button-->
                        <a class="activator p-3 mr-2"><i class="fa fa-share-alt white-text"></i></a>
                        <!--Title-->
                        <h4 class="card-title">Track</h4>
                        <hr class="hr-light">
                        <!--Text-->
                        <p class="font-small mb-4">Click here to check the status of your order. You'll be asked for the tracking name you entered during placing an order.</p>
                        
                            <a  class="btn blue-gradient btn-rounded" href="{{url('/track')}}">Track<span><i class="fa fa-chevron-right pl-2"></i></span></a>

                   

                    </div>
                    <!--/.Card content-->
                </div>
            </div>


        </div>


    </div>

</section>

<section class="container py-5">
    <ul class="nav nav-tabs nav-justified indigo" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#panel5" role="tab"><i class="fa fa-user"></i> Account</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel6" role="tab"><i class="fa fa-credit-card-alt"></i> Billing Help</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel7" role="tab"><i class="fa fa-cog"></i> Troubleshooting</a>
        </li>
    </ul>
    <br>
    <div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

        <!-- Accordion card -->
        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingOne">
                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <p class="mb-0">
                        Can you give me more details? <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx" >
                <div class="card-body">
                    I am just going to use your referral code and send you your rides. The amount of dollars that you'r going to get per referral can be seen in your free rides tab in-app. This changes countrywise.
                </div>
            </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingTwo">
                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <p class="mb-0">
                        Does this work worldwide? <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx" >
                <div class="card-body">
                    Yes.
                </div>
            </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        How do i apply this free ride? <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    Nothing goes here. It's automatically applied.
                </div>
            </div>
        </div>

        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        Is it legal or ca*d*d? <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    My service is legal and no blackhat activity is involved.
                </div>
            </div>
        </div>

        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        Will this get my account banned?  <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    No, this thing never happened in days gone by.
                </div>
            </div>
        </div>

        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        Do they stack?<i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    No, you will exhaust one referral per trip.
                </div>
            </div>
        </div>

        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        It's too expensive... <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    Paying 4$ for a 10-25$ ride isn't expensive.
                </div>
            </div>
        </div>

        <div class="card">

            <!-- Card header -->
            <div class="card-header" role="tab" id="headingThree">
                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <p class="mb-0">
                        How do i buy? <i class="fa fa-angle-down rotate-icon"></i>
                    </p>
                </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
                <div class="card-body">
                    Simply click the order button up above.
                </div>
            </div>
        </div>
        <!-- Accordion card -->
    </div>
</section>

<footer>
    <h6 class="footer-copyright grey-text text-center"><a href=""> Skype:</a><b>KissMyAxe.</b></h6>
</footer>

<!-- JS -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script type="text/javascript" src="js/pro.min.js"></script>
</body>
</html>



