<?php
error_reporting(0);
$conn = mysqli_connect("mysql.hostinger.co.uk", "u885804293_uber", "a1b2c3d4f5","u885804293_uber");


if(isset($_POST['track'])) {
	$tname = $_POST['tname'];
		if(!isset($_POST['tname'])) {
		$tok = 0;
		echo "<script type='text/javascript'>alert('Please fill in the tracking name.')</script>";
	}
	elseif(mysqli_num_rows(mysqli_query($conn,"SELECT * FROM orders WHERE name='$tname'")) !== 1) {
		$tok = 0;
		echo "<script type='text/javascript'>alert('Tracking name not found.')</script>";
	}
	else {
		$tok = 1;
		$find = mysqli_query($conn,"SELECT * FROM orders WHERE name='$tname'");
		$tdata = mysqli_fetch_array($find);
		if($tdata['pending'] == 0) {
			$tstatus = "Pending review <small>(needs 2 confirms)</small>";
		}
		elseif($tdata['pending'] == 1) {
			$tstatus = "Order confirmed";
		}
		elseif($tdata['pending'] == 2) {
			$tstatus = "Completed";
		}
		$tdataecho = "<b>Status: </b>" . $tstatus . "</p><b>Ordered: </b>" . $tdata['number'] . "</p><b>Delivered: </b>" . $tdata['delivered'];
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="author" content="Samuel Kronus" />

    <title>Uber Referrals &bull; KissMyAxe</title>

    <!-- CSS -->

	<link type="text/css" rel="stylesheet" href="css/pro.min.css"  media="screen,projection"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


	<!-- JS -->

</head>

<body style="background-image: url('img/bg.jpg');">


    <section class="container py-5">

		<div class="col-md-6 offset-md-4">

			<div class="card" >


				<div class="card-body">

					<?php
					if(!isset($_POST['track']) || $tok == 0) {
						?>

						<div class="row">
							<div class="col">
								<div class="form-header blue accent-3">
									<h4 class="text-center py-5">Track Your Order</h4>
									<a href="order.php" class="white-text">Click here to create a new order &rarr;</a>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col">
								<form action="" method="POST">

									<div class="form-row">
										<!-- Grid column -->
										<div class="col">
											<!-- Material input -->
											<div class="md-form form-group">
												<input type="text" class="form-control" id="inputEmail4MD" name="tname" placeholder="Order tracking name" required>
												<label for="inputEmail4MD">Order tracking name</label>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col">
											<button class="btn-outline-primary btn" type="submit" name="track" >TRACK</button><br>
										</div>
									</div><br>
									<div class="row d-flex align-items-center mb-4">

										<!--Grid column-->
										<div class="text-center mb-3 col-md-12">
											<p class="font-small d-flex justify-content-end">If you have any questions, add me on skype: <a href="#" class="green-text ml-1 font-weight-bold">kissmyaxe.</a></p>
										</div>
										<!--Grid column-->
									</div>
								</form>
							</div>
						</div>

						<?php
					}
					elseif($tok == 1) {
						?>

						<div class="form-header blue accent-3">
							<h3 class="text-center py-5">Tracking <?php echo $tname; ?></h3>
							<a href="track.php" class="white-text" >Click here to track a different order &rarr;</a>
						</div>

						<form action="" method="POST">
							<br>
							<?php echo $tdataecho; ?>
						</form>
						<?php
					}
					?>



				</div>


			</div>
		</div>



	</section>

	<footer>
		<h6 class="footer-copyright grey-text text-center"><a href=""> Skype:</a><b>KissMyAxe.</b></h6>
	</footer>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
	<script src="js-webshim/minified/polyfiller.js"></script>
	<script>
		webshim.activeLang('en');
		webshims.polyfill('forms');
		webshims.cfg.no$Switch = true;
	</script>
	<script type="text/javascript" src="js/pro.min.js"></script>
</body>
</html>	