
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="author" content="Samuel Kronus" />

    <title>Uber Referrals &bull; KissMyAxe</title>

    <!-- CSS -->
	<!-- CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/pro.min.css"  media="screen,projection"/>

</head>

<body style="background-image: url('img/bg.jpg');">



<!--Section: Contact v.3-->
<section class="py-5 container">

	<!--Grid row-->
	<div class="row">

		<!--Grid column-->
		<div class="col-md-12">

			<!--Form with header-->
			<div class="card">
				<div class="row">
					<div class="col-lg-8">
						<div class="card-body form" style="color:black;">
							<!--Header-->
							<div class="formHeader mb-1 pt-3">
								<h4 class="mb-5 mt-4 font-weight-bold"><i class="fa fa-envelope"></i> Request <a class="green-text font-weight-bold"><strong> Uber Rides</strong></a></h4>
								<a href="track.php">Click here to track your order &rarr;</a>
							</div>

							<br>
							@include('notification')
							<form method="POST" enctype="multipart/form-data" action="{{url('upload-order')}}">
								{{ csrf_field() }}


									<div class="alert alert-info" role="alert" style="background-color:lightblue; ">
										<p><strong>BTC: </strong>18HFpEaSdmYntowGZt8qHyZD39bjfUTQLM</p>
										<p> <strong>ETH: </strong>0xf246E183DEA70a8FF279239f796D7619beb8c689</p>
									</div>


									<!--Grid row-->
									<div class="row">

										<!--Grid column-->
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('name') ? ' has-error' : '' }}">
												<input type="text" id="Form-track" class="form-control" name="name" placeholder="Name to track order with (make it unique)" value="<?php if(isset($_SESSION['name'])) { echo $_SESSION['name']; }?>" required>
												<label for="Form-track">Name to track</label>
											</div>
										</div>
										<!--Grid column-->

										<!--Grid column-->
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('referral') ? ' has-error' : '' }}">
												<input type="text" id="Form-uber" class="form-control" name="referral" placeholder="Uber referral code" value="<?php if(isset($_SESSION['referral'])) { echo $_SESSION['referral']; }?>" required>
												<label for="Form-uber">Uber referral code</label>
											</div>
										</div>
										<!--Grid column-->

									</div>
									<!--Grid row-->


									<!--Grid row-->
									<div class="row">

										<!--Grid column-->
										<div class="col-md-12">

											<div class="md-form {{ $errors->has('number') ? ' has-error' : '' }}">
												<input type="text" id="Form-number" class="form-control" name="number" placeholder="Number of rides you paid for" value="<?php if(isset($_SESSION['amount'])) { echo $_SESSION['amount']; }?>" required>
												<label for="Form-number">Number of rides</label>
												{{--<button class="btn-floating btn-lg blue" type="submit" name="step1">--}}
													{{--<i class="fa fa-send-o"></i>--}}
												{{--</button>--}}
											</div>

										</div>
										<!--Grid column-->

									</div>
									<!--Grid row-->



									<div class="row">
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('hf') ? ' has-error' : '' }}">
												<input type="text" id="Form-trac" class="form-control" name="hf" placeholder="HF profile link (optional)" value="<?php if(isset($_SESSION['hflink'])) { echo $_SESSION['hflink']; }?>">
												<label for="Form-trac">HF Profile Link</label>
											</div>
										</div>
										<!--Grid column-->

										<!--Grid column-->
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('skype') ? ' has-error' : '' }}">
												<input type="text" id="Form-ub" class="form-control" name="skype" placeholder="Skype (optional)" value="<?php if(isset($_SESSION['skype'])) { echo $_SESSION['skype']; }?>">
												<label for="Form-ub">Skype</label>
											</div>
										</div>
										<!--Grid column-->

									</div><br>
									<div class="row">

										<!--Grid column-->
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('email') ? ' has-error' : '' }}">
												<input type="email" class="form-control" name="email" placeholder="Email" id="email">
												<label for="email">Email</label>
											</div>
										</div>
										{{--<div class="col-md-6">--}}
											{{--<div class="form-check">--}}
												{{--<input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1"  onclick="showmail()" >--}}
												{{--<label class="form-check-label" for="defaultCheckbox1">--}}
													{{--Get notifed on order completion via email--}}
												{{--</label>--}}
											{{--</div>--}}
										{{--</div>--}}
									</div><br><br>

									{{--<div class="row">--}}
										{{--<div class="col">--}}
											{{--<button class="btn btn-secondary" type="submit" name="step1back" onclick="unvalidate()">< BACK</button>--}}
											{{--<button class="btn btn-secondary" type="submit" name="step2">NEXT</button>--}}
										{{--</div>--}}


										{{----}}
									{{--</div>--}}


									<div class="row">
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('amount') ? ' has-error' : '' }}">
												<input type="text" class="form-control" id="value" name="amount" placeholder="Exact amount of btc sent" required>
												<label for="Form-uber">Amount of Btc sent</label>
											</div>
										</div>
										<div class="col-md-6">
											<div class="md-form {{ $errors->has('screenshot') ? ' has-error' : '' }}">
												<input type="text" class="form-control" id="pic" name="screenshot" placeholder="Screenshot of payment" required>
												<label for="Form-uber">Screenshot of payment</label>
											</div>
										</div>


									</div>
									<div class="row">
										<div class="col">
											<div class="md-form {{ $errors->has('blockchain') ? ' has-error' : '' }}">
												<input type="text" class="form-control" id="trans" name="blockchain" placeholder="Blockchain confirmation link (with https://)" value="<?php if(isset($_SESSION['transaction'])) { echo $_SESSION['transaction']; }?>" required>
												<label for="form-contact-company" class="">Blockchain confirmation link (with https://)</label>
											</div>

										</div>
									</div>
									<div class="row">
										<div class="col">
											{{--<button class="btn btn-secondary" type="submit" name="step2back" onclick="unvalidate2()">< BACK</button>--}}
											<button class="btn btn-secondary" type="submit" name="submit" href="{{url('confirmation-email')}}">SUBMIT</button>

										</div>
									</div><br>
									<div class="row">
										<div class="row d-flex align-items-center mb-4">

											<!--Grid column-->
											<div class="text-center mb-3 col-md-12">
												<p class="font-small d-flex justify-content-end">If you have any questions, add me on skype: <a href="#" class="green-text ml-1 font-weight-bold">kissmyaxe.</a></p>
											</div>
											<!--Grid column-->
										</div>
									</div>


							</form>

						</div>
					</div>

					<div class="col-lg-4">
						<div class="card-body contact text-center">
							<div class="mb-5">
								<h3>Contact information</h3>
							</div>

							<ul class="contact-icons list-unstyled ml-4">

								<li>

											<p><i class="fa fa-skype"></i><b></b>kissmyaxe.</p>
								</li>

								<li>

									<p><i class="fa fa-facebook"></i><b></b>kissmyaxe.</p>
								</li>
							</ul>

							<hr class="hr-light mb-4 mt-4">

							<ul class="list-inline text-center list-unstyled">


							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--/Form with header-->

		</div>
		<!--Grid column-->

	</div>
	<!--Grid row-->

</section>
<footer>
	<h6 class="footer-copyright grey-text text-center"><a href=""> Skype:</a><b>KissMyAxe.</b></h6>
</footer>





<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="js-webshim/minified/polyfiller.js"></script>
<script>
	webshim.activeLang('en');
	webshims.polyfill('forms');
	webshims.cfg.no$Switch = true;

	function showmail() {
		if (document.getElementById("mailCheck").checked) {
			document.getElementById("email").style.display = "block";
			document.getElementById("email").required = true;
		}
		else {
			document.getElementById("email").style.display = "none";
			document.getElementById("email").required = false;
		}

		window.onload = showmail();

	}
	function unvalidate() {
		document.getElementById("email").required = false;
	}
	function unvalidate2() {
		document.getElementById("value").required = false;
		document.getElementById("pic").required = false;
		document.getElementById("trans").required = false;
	}
</script>
	<script type="text/javascript" src="js/pro.min.js"></script>
</body>
</html>	