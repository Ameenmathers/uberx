
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name=viewport content='width=1800'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="author" content="Samuel Kronus" />

    <title>Uber Referrals &bull; KissMyAxe</title>

    <!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->

	<link type="text/css" rel="stylesheet" href="css/pro.min.css"  media="screen,projection"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



</head>

<body  style="background-image: url('img/bg.jpg');">

@include('notification')
{{--<section class="py-5 container">--}}

  {{--<div class="row ">--}}

	  {{--<div class="col-md-6 offset-md-4">--}}
		  {{--<!-- Card -->--}}
		  {{--<div class="card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg);">--}}

			  {{--<!-- Content -->--}}
			  {{--<div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">--}}
				  {{--<div>--}}
					  {{--<h5 class="blue-gradient-text"><i class="fa fa-user"></i> Administrator Panel - Login</h5>--}}
					  {{--<form action="" method="POST">--}}
						  {{--<br>--}}
						  {{--<input type="password" class="input-field col s6" name="adminPass" placeholder="Password" required></p>--}}
						  {{--<button class="btn blue-gradient btn-rounded" type="submit" name="checkPass">SUBMIT</button></p>--}}
					  {{--</form>--}}

				  {{--</div>--}}
			  {{--</div>--}}
			  {{--<!-- Content -->--}}
		  {{--</div>--}}
		  {{--<!-- Card -->--}}

	  {{--</div>--}}

  {{--</div>--}}






{{--</section>--}}



<section class="py-5 container">
	<div class="row">
		<div class="col-md-6 mb-4">

			<!--Card Primary-->
			<div class="card indigo text-center z-depth-2">
				<div class="card-body">
					<h3 class="card-header">Admin Panel
						<span class="text-lighten-4"></span>
					</h3><br>
					<form method="POST">

					</form>
				</div>
			</div>
			<!--/.Card Primary-->
			<br>
		</div>
	</div>

	<div class="row">

		<table class="table table-striped blue-grey lighten-4">
			<tr>
				<th>Name</th>
				<th>HF</th>
				<th>Skype</th>
				<th>Email</th>
				<th>Number of rides</th>
				<th>BTC Amount</th>
				<th>Referral Code</th>
				<th>Screenshot</th>
				<th>Blockchain</th>
				<th>Time</th>
				<th>Status</th>
				<th>Mail</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>

			@foreach( $orders as $order)

					<tr>


						<td>{{$order->name}}</td>
						<td>{{$order->hf}}</td>
						<td>{{$order->skype}}</td>
						<td>{{$order->email}}</td>
						<td>{{$order->number}}</td>
						<td>{{$order->amount}}</td>
						<td>{{$order->referral}}</td>
						<td>{{$order->screenshot}}</td>
						<td>{{$order->blockchain}}</td>
						<td>{{$order->timestamps}}</td>
						<td>{{$order->status}}</td>
						<td><a   class='btn btn-primary' href="{{url('update-order/'. $order->oid)}}">Update</a></td>
						<td><input type='submit' name='mail' value='Mail' class='btn btn-success'></td>
						<td><a type='submit' name='action' class='btn btn-danger' href="{{url('delete-order/' . $order->oid) }}">X</a></td>
				@endforeach
					</tr>
		</table>

	</div>
</section>



<section class="py-5">
	@include('notification')
	<div class="card container">
		<form method="POST" enctype="multipart/form-data" action="{{url('/update-order')}}">
			{{ csrf_field() }}



					<!--Grid row-->
			<div class="row">

				<!--Grid column-->
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('name') ? ' has-error' : '' }}">
						<input type="text" id="Form-track" class="form-control" name="name" placeholder="Name to track order with (make it unique)" value="<?php if(isset($_SESSION['name'])) { echo $_SESSION['name']; }?>" required>
						<label for="Form-track">Name to track</label>
					</div>
				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('referral') ? ' has-error' : '' }}">
						<input type="text" id="Form-uber" class="form-control" name="referral" placeholder="Uber referral code" value="<?php if(isset($_SESSION['referral'])) { echo $_SESSION['referral']; }?>" required>
						<label for="Form-uber">Uber referral code</label>
					</div>
				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->


			<!--Grid row-->
			<div class="row">

				<!--Grid column-->
				<div class="col-md-6">

					<div class="md-form {{ $errors->has('number') ? ' has-error' : '' }}">
						<input type="text" id="Form-number" class="form-control" name="number" placeholder="Number of rides you paid for" value="<?php if(isset($_SESSION['amount'])) { echo $_SESSION['amount']; }?>" required>
						<label for="Form-number">Number of rides</label>

					</div>

				</div>
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('hf') ? ' has-error' : '' }}">
						<input type="text" id="Form-trac" class="form-control" name="hf" placeholder="HF profile link (optional)" value="<?php if(isset($_SESSION['hflink'])) { echo $_SESSION['hflink']; }?>">
						<label for="Form-trac">HF Profile Link</label>
					</div>
				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->



			<div class="row">

				<!--Grid column-->

				<!--Grid column-->
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('skype') ? ' has-error' : '' }}">
						<input type="text" id="Form-ub" class="form-control" name="skype" placeholder="Skype (optional)" value="<?php if(isset($_SESSION['skype'])) { echo $_SESSION['skype']; }?>">
						<label for="Form-ub">Skype</label>
					</div>
				</div>
				<!--Grid column-->
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('email') ? ' has-error' : '' }}">
						<input type="email" class="form-control" name="email" placeholder="Email" id="email">
						<label for="email">Email</label>
					</div>
				</div>
			</div>
			<div class="row">

				<!--Grid column-->


			</div><br>



			<div class="row">
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('amount') ? ' has-error' : '' }}">
						<input type="text" class="form-control" id="value" name="amount" placeholder="Exact amount of btc sent" required>
						<label for="Form-uber">Amount of Btc sent</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="md-form {{ $errors->has('screenshot') ? ' has-error' : '' }}">
						<input type="text" class="form-control" id="pic" name="screenshot" placeholder="Screenshot of payment" required>
						<label for="Form-uber">Screenshot of payment</label>
					</div>
				</div>


			</div>
			<div class="row">
				<div class="col">
					<div class="md-form {{ $errors->has('blockchain') ? ' has-error' : '' }}">
						<input type="text" class="form-control" id="trans" name="blockchain" placeholder="Blockchain confirmation link (with https://)" value="<?php if(isset($_SESSION['transaction'])) { echo $_SESSION['transaction']; }?>" required>
						<label for="form-contact-company" class="">Blockchain confirmation link (with https://)</label>
					</div>
				</div><br>
				<button  name='submit'  class='btn btn-primary'>Update</button>
			</div>
		</form>

	</div>
</section>



<footer>
	<h6 class="footer-copyright grey-text text-center"><a href=""> Skype:</a><b>KissMyAxe.</b></h6>
</footer>



<script type="text/javascript" src="{{url('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js')}}"></script>
<script src="{{url('js-webshim/minified/polyfiller.js')}}"></script>
<script>
	webshim.activeLang('en');
	webshims.polyfill('forms');
	webshims.cfg.no$Switch = true;
</script>
<script type="text/javascript" src="{{url('js/pro.min.js')}}"></script>

</body>
</html>	