<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\Mail\ConfirmationMail;
use Illuminate\Support\Facades\Mail;


class PublicController extends Controller
{
    // main pages
    public function index()
    {


        return view('welcome',[


        ]);
    }

    public function logout() {
        Auth::logout();
        return redirect('/welcome');
    }

    public function postUploadOrder(Request $request){

       // try{
            $order = new order();
            $order->name= $request->input('name');
            $order->referral= $request->input('referral');
            $order->number = $request->input('number');
            $order->hf= $request->input('hf');
            $order->skype= $request->input('skype');
            $order->amount= $request->input('amount');
            $order->screenshot= $request->input('screenshot');
            $order->blockchain= $request->input('blockchain');
            $order->email= $request->input('email');
            $order->save();

        $email = $request->input('email');
        Mail::to($email)->send(new ConfirmationMail($order));



            $request->session()->flash('success','order Added.');

            return redirect( '/order');

       // }catch (\Exception $exception) {
         //   $request->session()->flash( 'error', 'Sorry an error occurred. Please try again' );
      //      return redirect( '/order' );
      //  }


    }

    public function order()
    {
        return view('order');
    }

    public function track()
    {
        return view('track');
    }

    public function update()
    {
        return view('/update');
    }

	public function updateOrder($oid) {
		$order = order::find($oid);

		return view('update',[
			'order' => $order
		]);
    }

    public function admin()
    {
        $orders = order::all();

        return view('admin',[
                'orders' => $orders

            ]
        );
    }

    public function deleteOrder(Request $request, $oid)
    {
        order::destroy($oid);

        $request->session()->flash('success','Order Deleted.');
        return redirect('admin-dashboard');
    }




    public function postUpdateOrder(Request $request)
    {
        try{
	        $oid = $request->input('oid');
            $order = order::find($oid);
            $order->name = $request->input('name');
            $order->hf = $request->input('hf');
            $order->skype = $request->input('skype');
            $order->referral = $request->input('referral');
            $order->number = $request->input('number');
            $order->amount = $request->input('amount');
            $order->screenshot = $request->input('screenshot');
            $order->blockchain = $request->input('blockchain');
            $order->email = $request->input('email');
            $order->status = 'pending';
            $order->save();
            $request->session()->flash('success','Order Updated');
        } catch(\Exception $exception){
            $request->session()->flash('error','Sorry An Error Occurred.');
        }

        return redirect('/admin-dashboard');
    }


}
