<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('name');
            $table->string('email',125)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email',125)->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('oid');
            $table->string('name');
            $table->string('hf');
            $table->string('skype');
            $table->string('number');
            $table->string('amount');
            $table->string('referral');
            $table->string('screenshot');
            $table->string('blockchain');
            $table->enum('status', array('Pending','Delivered'));
            $table->string('email',125)->unique();
            $table->timestamps();
        });
        //

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('orders');



        //
    }
}
